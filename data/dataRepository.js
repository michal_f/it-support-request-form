const sqlite3 = require('sqlite3').verbose();

queryHandler = (query, parameters) => {
    return new Promise((resolve, reject) => {
        const db = new sqlite3.Database('./db/tickets.db', sqlite3.OPEN_READWRITE, (error) => {
            db.get("PRAGMA foreign_keys = ON")
            if (error) {
                reject(error);
                return;
            }
        });

        db.all(query, parameters, (error, data) => {
            // console.log('query:', query, parameters)
            if (error) {
                reject(error);
            } else {
                resolve(data);
            }
            db.close();
        });
    })
}

module.exports = {
    queryHandler
}