const {
    listAllTickets,
    addTicket,
    listSelectedTicket,
    listTicketsByAuthor,
    listTicketsByBranch,
    closeTicket,
} = require('./ticketController');
const { signIn } = require('../auth/signIn')
const { verifyTokenSignNew } = require('../auth/verifyToken')
const { createNewUser } = require('../auth/createUser')

startTicketRouting = (app) => {
    app.post('/addticket', verifyTokenSignNew, addTicket)
    app.get('/tickets', verifyTokenSignNew, listAllTickets)
    app.get('/tickets/:ticketID', verifyTokenSignNew, listSelectedTicket)
    app.get('/usertickets/:authorID', verifyTokenSignNew, listTicketsByAuthor)
    app.get('/branchtickets/:branchID', verifyTokenSignNew, listTicketsByBranch)
    app.put('/closeticket/:ticketID', verifyTokenSignNew, closeTicket)
}

startAdminRouting = (app) => {
    app.post('/createuser', verifyTokenSignNew, createNewUser)
}

startAuthRouting = (app) => {
    app.get('/signin', signIn)
}

module.exports = {
    startTicketRouting,
    startAdminRouting,
    startAuthRouting
}