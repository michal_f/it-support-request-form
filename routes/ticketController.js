const { queryHandler } = require('../data/dataRepository')
const obj1 = require('../server');
const obj2 = { bratyslawa: { a: 5 } }
const inside1 = obj1.locals
const inside2 = obj2.bratyslawa

addTicket = (req, res) => {
    const {
        authorID,
        cathegoryID,
        highPriority,
        content
    } = req.body
    const date = new Date;
    const creationDate = date.getTime();

    queryHandler(`INSERT INTO tickets (
        cathegory_ID,
        author_ID,
        creation_Date,
        high_priority,
        content) 
        VALUES ($1, $2, $3, $4, $5)`,
        [cathegoryID, authorID, creationDate, highPriority, content])
        .then(data => res.json({
            token: res.locals.token,
            data
        }), error => console.log(error))
}

listAllTickets = (req, res) => {
    console.log(obj1)
    queryHandler(`SELECT * FROM tickets`)
        .then(data => res.json({
            token: res.locals.token,
            data
        }), error => console.log(error))
}

listSelectedTicket = (req, res) => {
    const ticketID = req.params.ticketID
    queryHandler(`SELECT tickets.ID, tickets.author_ID, tickets.cathegory_ID, tickets.is_opened, tickets.high_priority, 
    tickets.creation_date, tickets.closed_by, tickets.closing_date, tickets.content,
    users.name as author, libraries.name as branch FROM tickets
    INNER JOIN users ON users.ID = author_ID
    INNER JOIN libraries ON libraries.ID = users.libraryID
    WHERE tickets.ID = $1`, [ticketID])
    .then(data => res.json({
        token: res.locals.token,
        data
    }), error => console.log(error))
}

listTicketsByAuthor = (req, res) => {
    const authorID = req.params.authorID;
    queryHandler(`Select ID, author_ID, content FROM tickets WHERE author_ID = $1`, authorID)
    .then(data => res.json({
        token: res.locals.token,
        data
    }), error => console.log(error))
}

listTicketsByBranch = (req, res) => {
    const branchID = req.params.branchID;
    queryHandler(`Select tickets.ID, author_ID, content FROM tickets INNER JOIN users ON users.ID = tickets.author_ID WHERE libraryID = $1`, branchID)
    .then(data => res.json({
        token: res.locals.token,
        data
    }), error => console.log(error))
}

closeTicket = (req, res) => {
    const ticketID = req.params.ticketID;
    queryHandler(`UPDATE tickets SET is_opened = 0 WHERE ID = $1`, ticketID)
    .then(data => res.json({
        token: res.locals.token,
        data
    }), error => console.log(error))
}

module.exports = {
    addTicket,
    listAllTickets,
    listSelectedTicket,
    listTicketsByBranch,
    listTicketsByAuthor,
    closeTicket
};