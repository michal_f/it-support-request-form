const jwt = require('jsonwebtoken');
const jwtSecret = 'secret'

verifyTokenSignNew = (req, res, next) => {
    if (req.headers.authorisation === undefined) res.sendStatus(401);
    const bearerToken = req.headers.authorisation.split(' ')[1];
    if (bearerToken === undefined) res.sendStatus(401);
    
    jwt.verify(bearerToken, jwtSecret, (err, decoded) => {
        if (err) {
            console.log('error: ', err);
            res.sendStatus(401);
        } else {
            const expirationTime = req.app.locals.config.session_length;
            const userData = { libraryID: decoded.libraryID, name: decoded.name, email: decoded.email, accessLvl: decoded.accessLvl }
            res.locals.token = jwt.sign(userData, jwtSecret, { expiresIn: expirationTime })
            next();
        }
    })
}

module.exports = {
    verifyTokenSignNew
}