const { queryHandler } = require('../data/dataRepository')
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const jwtSecret = 'secret'

signIn = (req, res, next) => {
    const { name, password } = req.body;
    const expirationTime = req.app.locals.config.session_length;

    return getUserFromDatabase(name)
        .then(userList => {
            return verifyLogin(userList, password)
        })
        .then(userData => {
            return signToken(userData, expirationTime)
        })
        .then(token => {
            res.json({token})
        })
        .catch(error => {
            console.log('error at the end: ', error);
            res.status(400).send("Bad username or password")
        })
}

getUserFromDatabase = (name) => {
    return queryHandler(`SELECT libraryID, name, password, email, accessLvl FROM users WHERE name = $1`, [name]);
}

verifyLogin = (userList, enteredPassword) => {
    const user = userList[0];
    if (!user) throw 'user with given name does not exist';
    return bcrypt.compare(enteredPassword, user.password)
        .then(result => {
            if (!result) throw 'wrong password';
            return { libraryID: user.libraryID, name: user.name, email: user.email, accessLvl: user.accessLvl }
        })
}

signToken = (userData, expirationTime) => {
    return jwt.sign(userData, jwtSecret, { expiresIn: expirationTime });
}

module.exports = {
    signIn
}