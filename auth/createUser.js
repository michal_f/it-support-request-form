const { queryHandler } = require('../data/dataRepository')
const bcrypt = require('bcrypt');
const saltRounds = 10;

createNewUser = (req, res) => {
  const { library, email, name, password, accessLevel } = req.body;
  return bcrypt.hash(password, saltRounds)
    .then(hashedPassword => {
      return addUserToDatabase(library, email, name, hashedPassword, accessLevel)
    })
    .then(user => {
      res.send({
        message: 'User created',
      })
    })
    .catch(error => {
      console.log('Error on createNewUser: ', error)
      res.send('Error - user not created')
    });
}

addUserToDatabase = (library, email, name, hashedPassword, accessLevel) => {
  return queryHandler(`INSERT INTO users (
        libraryID,
        email,
        name,
        password,
        accessLvl)
        VALUES ($1, $2, $3, $4, $5)`,
    [library, email, name, hashedPassword, accessLevel])
}

module.exports = {
  createNewUser
}
