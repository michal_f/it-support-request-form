const fs = require('fs');
const express = require('express');
const https = require('https');
const http = require('http');
const cors = require('cors');
const app = express();

const { queryHandler } = require('./data/dataRepository');
const {
    startTicketRouting,
    startAdminRouting,
    startAuthRouting } = require('./routes/routing');

app.use(express.json());
app.use(cors());

startTicketRouting(app);
startAdminRouting(app);
startAuthRouting(app);

module.exports = {app}
