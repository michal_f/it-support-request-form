const {app} = require('./server')
const port = process.env.PORT;
// const port = 3001;
const { queryHandler } = require('./data/dataRepository');


queryHandler(`SELECT * FROM config`).then(config => {
    app.locals.config = config[0];
    app.listen(port, () => console.log(`Hello Dave, I am listening at port ${port} \nMOTD: "${app.locals.config.motd}"`));
})
    .catch(error => console.log('Unable to start application: ', error))