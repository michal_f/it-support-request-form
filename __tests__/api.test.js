const { app } = require('../server')
const supertest = require('supertest')
const request = supertest(app)

describe('ticketRouting Tests', () => {

    it('should return 401 - unauthorised, when trying to get /tickets without authorisation', async done => {
        // Sends GET Request to /test endpoint
        const res = await request.get('/tickets')

        expect(res.status).toBe(401);
        done()
    })
})